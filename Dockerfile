# Base image
FROM ruby:2.5.3-alpine3.7 AS base

WORKDIR /app

RUN apk update && apk add --no-cache \
    build-base \
    tzdata \
    linux-headers \
    postgresql-dev \
    postgresql-client \
    libcurl \
    nodejs

COPY Gemfile* ./

# Development image
FROM base AS development

RUN bundle install

COPY . ./
