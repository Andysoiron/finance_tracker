# frozen_string_literal: true
require 'csv'

class TransactionsImporter::SpardaBank < TransactionsImporter::Default
  def call
    data.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
    CSV.parse(data.lines[11..-1].join, col_sep: ';') do |row|
      Transaction.create!(row_to_hash(row)) rescue ActiveRecord::RecordNotUnique
    end
  end

  private

  def row_to_hash(row)
    {
      booking_date: row[0],
      payment_date: row[1],
      purpose: row[2],
      value: row[3].gsub(',', '').gsub('.', ''),
      currency: row[4],
      account_id: account_id
    }
  end
end
