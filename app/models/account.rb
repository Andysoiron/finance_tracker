class Account < ApplicationRecord
  has_many :transactions
  include Balanceable

  def provider
    Provider.new(provider_slug)
  end
end
