# frozen_string_literal: true

class Provider
  attr_reader :slug

  IMPORTERS_MAP = {
    sparda_bank: TransactionsImporter::SpardaBank
  }
  NAMES = {
    sparda_bank: 'Sparda Bank'
  }
  KNOWN_PROVIDERS = %i[sparda_bank]

  def self.all
    KNOWN_PROVIDERS.map {|provider_slug| new(provider_slug) }
  end

  def initialize(slug)
    if slug.nil?
      @slug = :default
    else
      @slug = slug.to_sym
    end
  end

  def transactions_importer
    IMPORTERS_MAP[slug] || TransactionsImporter::Default
  end

  def name
    NAMES[slug] || 'Default'
  end
end
