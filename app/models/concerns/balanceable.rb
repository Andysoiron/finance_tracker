# frozen_string_literal: true

module Balanceable
  extend ActiveSupport::Concern

  def balance
    transactions.map(&:value).reduce(:+) || 0
  end
end
