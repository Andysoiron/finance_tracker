class Category < ApplicationRecord
  has_many :transactions
  include Balanceable
end
