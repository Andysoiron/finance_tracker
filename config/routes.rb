Rails.application.routes.draw do
  root to: 'categories#index'

  resources :transactions, except: :show
  resources :categories, only: [:show, :create, :index, :new]
  resources :accounts, except: :destroy
end
