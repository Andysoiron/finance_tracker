# frozen_string_literal: true

namespace :transactions do
  desc "Import transactions from transactions csv"
  task import: :environment do
    account = Account.find_by_name(ENV['account'])
    file = File.read(ENV['path'])
    account.provider.transactions_importer.call(file, account.id)
  end
end
