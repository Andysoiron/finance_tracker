## Can read bank exports and helps catogries transactions

I build this tool to keep track of my expenses without giving a third party access to my bank account. Maybe it is useful for you as well.

### Loading transactions:

Currently it can only read a transactons export from german bank Sparda-Bank.

Export transactions data from your online banking and run `$ rake transactions:import account=Account_name path=path/to/transactions.csv`
