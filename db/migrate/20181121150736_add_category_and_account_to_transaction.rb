class AddCategoryAndAccountToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :category_id, :integer
    add_column :transactions, :account_id, :integer, null: false
    add_index :transactions, [:booking_date, :purpose, :value, :account_id], unique: true, name: 'unique_transaction_index'
  end
end
