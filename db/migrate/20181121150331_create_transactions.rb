class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.date :booking_date
      t.date :payment_date
      t.string :purpose
      t.integer :value
      t.string :currency

      t.timestamps
    end
  end
end
