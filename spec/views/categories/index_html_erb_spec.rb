# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'categories/index.html.erb' do
  subject { render }

  let(:category) { create(:category, name: 'Food') }
  let!(:transaction) { create(:transaction, category: category, value: 1000, booking_date: DateTime.new(2019)) }
  let!(:other_transaction) { create(:transaction, category: category, value: 1000, booking_date: DateTime.new(2018)) }

  before do
    assign(:categories, [category])
  end

  it 'has the category name' do
    expect(subject).to include('Food')
  end

  it 'sums the transactions' do
    expect(subject).to include('20.00')
  end

  it 'has a year selector that only shows years with transactions' do
    expect(subject).to include('<option value="">all</option>')
    expect(subject).to include('<option value="2019">2019</option>')
    expect(subject).to include('<option value="2018">2018</option>')
    expect(subject).to_not include('<option value="2019">2017</option>')
  end

  it 'has a month selector that shows all the months' do
    expect(subject).to include('<option value="">all</option>')
    expect(subject).to include('<option value="1">January</option>')
    expect(subject).to include('<option value="2">February</option>')
    expect(subject).to include('<option value="3">March</option>')
    expect(subject).to include('<option value="4">April</option>')
    expect(subject).to include('<option value="5">May</option>')
    expect(subject).to include('<option value="6">June</option>')
    expect(subject).to include('<option value="7">July</option>')
    expect(subject).to include('<option value="8">August</option>')
    expect(subject).to include('<option value="9">September</option>')
    expect(subject).to include('<option value="10">October</option>')
    expect(subject).to include('<option value="11">November</option>')
    expect(subject).to include('<option value="12">December</option>')
  end
end
