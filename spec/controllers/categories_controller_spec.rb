# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe 'GET index' do
    subject { get :index, params: params }
    let(:params) { {} }
    let!(:category) { create(:category)}
    let!(:transaction) { create(:transaction, category: category) }

    it { is_expected.to have_http_status :success }
    it 'shows all categories' do
      subject
      expect(assigns(:categories)).to contain_exactly(category)
    end

    context 'with year filter' do
      before { Timecop.freeze(Time.new(2019, 3)) }
      after { Timecop.return }
      let(:params) { { year: year } }
      let(:year) { DateTime.now.year }
      let(:last_year_category) { create(:category) }
      let!(:last_year_transaction) { create(:transaction, category: last_year_category, booking_date: date_last_year) }
      let(:date_last_year) { (DateTime.now - 1.year).to_date }
      let(:last_month_category) { create(:category) }
      let!(:last_month_transaction) { create(:transaction, category: last_month_category, booking_date: date_last_month) }
      let(:date_last_month) { (DateTime.now - 1.month).to_date }

      it 'only shows filtered categories' do
        subject
        expect(assigns(:categories)).to contain_exactly(category, last_month_category)
      end

      context 'when filtered for all years' do
        let(:year) { nil }

        it 'shows all categories' do
          subject
          expect(assigns(:categories)).to contain_exactly(category, last_year_category, last_month_category)
        end
      end

      context 'and month filter' do
        let(:params) { { year: year, month: month } }
        let(:month) { DateTime.now.month}

        it 'shows just this months categories' do
          subject
          expect(assigns(:categories)).to contain_exactly(category)
        end

        context 'when filtered for all months' do
          let(:month) { nil }

          it 'shows all categories from this year' do
            subject
            expect(assigns(:categories)).to contain_exactly(category, last_month_category)
          end
        end
      end
    end
  end
end
