# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TransactionsImporter::SpardaBank do
  describe '.call' do
    subject { described_class.call(csv_string, account.id) }
    let(:csv_string) do
      File.read('spec/fixtures/sparda_bank_sample.csv')
    end
    let!(:account) { create(:account) }

    specify do
      expect { subject }.to change{Transaction.count}.by(6)
    end

    context 'if the transactions already exist' do
      specify do
        subject
        expect { subject }.to_not change{Transaction.count}
      end
    end
  end
end
