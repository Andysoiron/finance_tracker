# frozen_string_literal: true

FactoryBot.define do
  factory :transaction do
    booking_date { Date.today }
    payment_date { Date.today }
    purpose { 'I bought something' }
    value { 20000 }
    currency { 'EUR' }
    association :account, factory: :account
  end
end
