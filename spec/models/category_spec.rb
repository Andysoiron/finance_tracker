# frozen_string_literal: true

require 'rails_helper'

describe Category do
  describe '#balance' do
    subject { category.balance }
    let(:category) { create(:category) }

    it { is_expected.to eq(0) }

    context 'with transactions' do
      let!(:transaction_1) { create(:transaction, value: 20_00, category: category) }
      let!(:transaction_2) { create(:transaction, value: 30_00, category: category) }

      it { is_expected.to eq(50_00) }
    end
  end
end
