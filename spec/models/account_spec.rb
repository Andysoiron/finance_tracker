# frozen_string_literal: true

require 'rails_helper'

describe Account do
  describe '#balance' do
    subject { account.balance }
    let(:account) { create(:account) }

    it { is_expected.to eq(0) }

    context 'with transactions' do
      let!(:transaction_1) { create(:transaction, value: 20_00, account: account) }
      let!(:transaction_2) { create(:transaction, value: 30_00, account: account) }

      it { is_expected.to eq(50_00) }
    end
  end

  describe '#provider' do
    subject { described_class.new(provider_slug: provider_slug).provider }
    let(:provider_slug) { 'sparda_bank' }

    it { is_expected.to be_kind_of(Provider) }
  end
end
